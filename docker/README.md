# Bytesweep Docker Containers

## Dev Environment Setup

NOTE: this will change (get easier) when the repos are made public

NOTE: if you have something else listening on ports 8000 or 5432 this will fail.

1. clone this repo
```
git clone git@gitlab.com:bytesweep/bytesweep.git
cd bytesweep
```

2. generate ssh key with no password. Add this key to gitlab. Place key in appropriate locations.
```
ssh-keygen # set key location to id_rsa (current directory)
cp id_rsa bytesweep-web
cp id_rsa bytesweep-worker
cp id_rsa bytesweep-cvefetch
cp id_rsa bytesweep-watchdog
```

3. Build docker containers. This may take awhile.
```
./build-bytesweep.sh
```

4. Start docker containers
```
./start-bytesweep.sh
```

5. Navigate to bytesweep at http://127.0.0.1:8000
